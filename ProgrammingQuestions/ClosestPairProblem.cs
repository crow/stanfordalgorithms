﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ProgrammingQuestions
{
    class ClosestPairProblem
    {
        public static Tuple<Point, Point> ClosestPair(List<Point> sourceList)
        {
            sourceList.Sort((p1, p2) => p1.X.CompareTo(p2.X));
            var Px = new List<Point>();
            Px.AddRange(sourceList);
            //sourceList.Sort((p1, p2) => p1.Y.CompareTo(p2.Y));
            //var Py = new List<Point>();
            //Py.AddRange(sourceList);

            if (sourceList.Count == 2) return new Tuple<Point, Point>(sourceList[0], sourceList[1]);
            if (sourceList.Count == 1) return new Tuple<Point, Point>(sourceList[0], new Point { X = int.MaxValue, Y = int.MaxValue });

            var splitIndex = sourceList.Count / 2;
            var left = sourceList.GetRange(0, splitIndex);
            var right = sourceList.GetRange(splitIndex, sourceList.Count - splitIndex);

            var closestPairOnLeft = ClosestPair(left);
            var closestPairOnRight = ClosestPair(right);

            var closestPairOnLeftOrRight = TupleExtensions.FindClosestPair(closestPairOnLeft, closestPairOnRight);
            var splitClosestPair = ClosestSplitPair(left, right, closestPairOnLeftOrRight.EuclideanDistance());

            return TupleExtensions.FindClosestPair(closestPairOnLeftOrRight, splitClosestPair);
        }
        
        private static Tuple<Point, Point> ClosestSplitPair(List<Point> left, List<Point> right, double delta)
        {
            #region
            //var xbar = left[left.Count - 1];
            //var leftBound = xbar.X - delta;
            //var rightBound = xbar.X + delta;
            //var sparseBox = left.Where(point => point.X >= leftBound).ToList(); //Sy
            //sparseBox.AddRange(right.Where(point => point.X <= rightBound));
            //// inefficient
            //sparseBox.Sort((p1, p2) => p1.Y.CompareTo(p2.Y)); 
            #endregion
            #region
            //if (left.Count > 8)
            //{
            //    left = left.GetRange(left.Count - 8, 8);
            //}
            //if (right.Count > 8)
            //{
            //    right = right.GetRange(0, 8);
            //} 
            //var minDistance = double.PositiveInfinity;
            //var mini = -1;
            //var minj = -1;
            //for (var i = 0; i < left.Count; i++)
            //{
            //    for (var j = 0; j < right.Count; j++)
            //    {
            //        var distance = Math.Sqrt(Math.Pow(left[i].X - right[j].X, 2) + Math.Pow(left[i].Y - right[j].Y, 2));
            //        if (distance < minDistance)
            //        {
            //            minDistance = distance;
            //            mini = i;
            //            minj = j;
            //        }
            //    }
            //}
            //return new Tuple<Point, Point>(left[mini], right[minj]);
            #endregion
            var xBar = left.Last().X;
            left = left.Where(p => Math.Abs(xBar - p.X) <= delta).ToList();
            right = right.Where(p => Math.Abs(xBar - p.X) <= delta).ToList();
            var minDistance = double.PositiveInfinity;
            var mini = -1;
            var minj = -1;
            for (var i = 0; i < left.Count; i++)
            {
                for (var j = 0; j < right.Count; j++)
                {
                    var distance = Math.Sqrt(Math.Pow(left[i].X - right[j].X, 2) + Math.Pow(left[i].Y - right[j].Y, 2));
                    if ((distance >= minDistance) || (double.IsInfinity(distance) || double.IsInfinity(minDistance)))
                        continue;
                    minDistance = distance;
                    mini = i;
                    minj = j;
                }
            }
            return (mini != -1 && minj != -1) ? new Tuple<Point, Point>(left[mini], right[minj]) : new Tuple<Point, Point>(new Point { X = 0, Y = 0 }, new Point { X = int.MaxValue, Y = int.MaxValue });
        }
    }
}
