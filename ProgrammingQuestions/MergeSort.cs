﻿using System.Collections.Generic;

namespace ProgrammingQuestions
{
    class MergeSort
    {
        public static List<int> Sort(List<int> listToSort)
        {
            if (listToSort.Count <= 1) return listToSort;
            var splitIndex = listToSort.Count / 2;
            var leftListPart = Sort(listToSort.GetRange(0, splitIndex));
            var rightListPart = Sort(listToSort.GetRange(splitIndex, listToSort.Count - splitIndex));
            return Merge(leftListPart, rightListPart);
        }

        private static List<int> Merge(List<int> leftListPart, List<int> rightListPart)
        {
            var elementsCount = leftListPart.Count + rightListPart.Count;
            var mergedArray = new List<int>();
            var i = 0;
            var j = 0;
            for (var k = 0; k < elementsCount; k++)
            {
                if (i >= leftListPart.Count || j >= rightListPart.Count) continue;
                if (leftListPart[i] < rightListPart[j])
                {
                    mergedArray.Add(leftListPart[i]);
                    i++;
                }
                else
                {
                    if (j < rightListPart.Count)
                    {
                        mergedArray.Add(rightListPart[j]);
                        j++;
                    }
                }
            }
            if (j < rightListPart.Count)
            {
                mergedArray.AddRange(rightListPart.GetRange(j, rightListPart.Count - j));
            }
            if (i < leftListPart.Count)
            {
                mergedArray.AddRange(leftListPart.GetRange(i, leftListPart.Count - i));
            }
            return mergedArray;
        }
    }
}
