﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ProgrammingQuestions
{
    class QuickSortProblem
    {
        public static QuickSortData QuickSort(List<int> sourceList, IPivotChoosingStrategy strategy)
        {
            if (sourceList.Count <= 1)
            {
                return new QuickSortData { ComparisonsCount = 0, DataArray = sourceList };
            }
            var pivotElement = strategy.ChoosePivot(sourceList);
            if (sourceList[0] != pivotElement)
            {
                sourceList.Swap(0, sourceList.IndexOf(pivotElement));
            }
            var partitionedList = Partition(sourceList, pivotElement);
            var comparisonsCount = sourceList.Count - 1;

            var splitIndex = partitionedList.IndexOf(pivotElement);
            var lessThenPivotList = partitionedList.GetRange(0, splitIndex);
            var greaterThenPivotList = partitionedList.GetRange(splitIndex + 1, partitionedList.Count - splitIndex - 1);

            var sortedLessPart = QuickSort(lessThenPivotList, strategy);
            var sortedGreaterPart = QuickSort(greaterThenPivotList, strategy);

            var sortedList = new List<int>();
            sortedList.AddRange(sortedLessPart.DataArray);
            sortedList.Add(pivotElement);
            sortedList.AddRange(sortedGreaterPart.DataArray);

            //Console.WriteLine("\tSorted array : " + sortedArray.Aggregate(string.Empty, (msg, current) => msg + (current + " ")));
            return new QuickSortData { ComparisonsCount = comparisonsCount + sortedGreaterPart.ComparisonsCount + sortedLessPart.ComparisonsCount, DataArray = sortedList };
        }

        // in-place partition
        public static List<int> Partition(List<int> sourceList, int pivot)
        {
            //Console.WriteLine("\tPartitioning with p=[" + pivot + "] : " + sourceList.Aggregate(string.Empty, (msg, current) => msg + (current + " ")));
            var pivotIndex = sourceList.IndexOf(pivot);
            for (var i = pivotIndex + 1; i < sourceList.Count; i++)
            {
                for (var j = i; j < sourceList.Count; j++)
                {
                    if (sourceList[j] > pivot) { /*nothing to do*/ }
                    if (sourceList[j] < pivot)
                    {
                        sourceList.Swap(i, j);
                        i++;
                    }
                    if (j != sourceList.Count - 1) continue;
                    sourceList.Swap(pivotIndex, i - 1);
                    i = sourceList.Count;
                }
            }
            //Console.WriteLine("\t\tResult : " + sourceList.Aggregate(string.Empty, (msg, current) => msg + (current + " ")));
            return sourceList;
        }
    }

    interface IPivotChoosingStrategy
    {
        int ChoosePivot(List<int> sourceList);
    }

    public class SelectFirstElementAsPivot : IPivotChoosingStrategy
    {
        public int ChoosePivot(List<int> sourceList)
        {
            return sourceList.First();
        }
    }

    public class SelectLastElementAsPivot : IPivotChoosingStrategy
    {
        public int ChoosePivot(List<int> sourceList)
        {
            return sourceList.Last();
        }
    }

    public class SelectByMedianOfThreePivotRule : IPivotChoosingStrategy
    {
        public int ChoosePivot(List<int> sourceList)
        {
            var three = new List<int> { sourceList.First(), sourceList.Middle(), sourceList.Last() };
            return three.MedianOfThree();
        }
    }

    // 1973 Blum-Floyd-Pratt-Rivest-Tarjan
    public class MedianOfMediansDeterministicPivot : IPivotChoosingStrategy
    {
        public int ChoosePivot(List<int> sourceList)
        {
            var groupSize = 5;
            var groupsCount = Math.Ceiling((double)sourceList.Count/5);
            var firstRoundMedians = new List<int>();
            for (var i = 0; i < groupsCount; i++)
            {
                var splitStart = i*groupSize;
                var splitEnd = (i + 1)*groupSize;
                if (splitEnd > sourceList.Count)
                {
                    groupSize = sourceList.Count - splitStart;
                }
                var currentGroup = sourceList.GetRange(splitStart, groupSize);
                currentGroup.Sort();
                firstRoundMedians.Add(currentGroup.Middle());
            }
            firstRoundMedians.Sort();
            return firstRoundMedians.Middle();
        }
    }
}
