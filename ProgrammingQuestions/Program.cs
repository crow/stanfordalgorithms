﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;

namespace ProgrammingQuestions
{
    class Program
    {
        static void Main()
        {
            const int taskIndex = 7;
            // 0 - Merge Sort
            // 1 - Inversions
            // 2 - Closest pair
            // 3 - QuickSort
            // 4 - Randomized Selection
            // 5 - Graph Min Cut
            // 6 - Strongly connected components
            // 7 - Hashes
            // 8 - Medians
            switch (taskIndex)
            {
                case 0:
                    {
                        Console.WriteLine("Q1.0 : Merge Sort");
                        var sourceList = new List<int> { 1, 5, 8, 12, 2, 5 };
                        Console.WriteLine("\tSource array : " + sourceList.Aggregate(string.Empty, (msg, current) => msg += current + " "));
                        var sortedList = MergeSort.Sort(sourceList);
                        Console.WriteLine("\tSorted array : " + sortedList.Aggregate(string.Empty, (msg, current) => msg += current + " "));
                        break;
                    }
                case 1:
                    {
                        Console.WriteLine("Q1.1 : Counting inversions");
                        var sourceArray = Helpers.GetArrayFromFile("ProgrammingQuestion-1 Q1 IntegerArray.txt");
                        var result = Inversions.SortAndCountInversions(sourceArray);
                        Console.WriteLine("Inversions count : " + result.InversionsCount);
                        break;
                    }
                case 2:
                    {
                        Console.WriteLine("Q1.2* : Finding closest pair");
                        var testPoints = new List<Point>();
                        #region Manual tests
                        //testPoints.Add(new Point { X = -10, Y = 0 });
                        //testPoints.Add(new Point { X = 0, Y = 0 });
                        //testPoints.Add(new Point { X = 10, Y = 0 });
                        //testPoints.Add(new Point { X = 20, Y = 0 });
                        //testPoints.Add(new Point { X = 30, Y = 0 });
                        //testPoints.Add(new Point { X = 40, Y = 0 });
                        //testPoints.Add(new Point { X = 50, Y = 0 });
                        //testPoints.Add(new Point { X = 60, Y = 0 });
                        //testPoints.Add(new Point { X = 70, Y = 0 });
                        //testPoints.Add(new Point { X = 80, Y = 0 });
                        //testPoints.Add(new Point { X = 90, Y = 0 });
                        //testPoints.Add(new Point { X = 91, Y = 0 });
                        //testPoints.Add(new Point { X = 110, Y = 0 });
                        //testPoints.Add(new Point { X = 120, Y = 0 });
                        //testPoints.Add(new Point { X = 130, Y = 0 });
                        //testPoints.Add(new Point { X = 140, Y = 0 });
                        //testPoints.Add(new Point { X = 150, Y = 0 });
                        //testPoints.Add(new Point { X = 160, Y = 0 });
                        //testPoints.Add(new Point { X = 170, Y = 0 });
                        //testPoints.Add(new Point { X = 180, Y = 0 }); 
                        #endregion
                        var random = new Random();
                        for (var i = 0; i < 20; i++)
                        {
                            var randomX = random.Next(50);
                            var randomY = random.Next(50);
                            testPoints.Add(new Point { X = randomX, Y = randomY });
                        }
                        var diag = testPoints.Aggregate(string.Empty, (s, current) => s + "(" + current.X + "," + current.Y + ")\r\n");
                        Console.WriteLine(diag);
                        var closestPair = ClosestPairProblem.ClosestPair(testPoints);
                        Console.WriteLine("Closest pair : " + closestPair);
                        break;
                    }
                case 3:
                    {
                        Console.WriteLine("Q2.1 : QuickSort");
                        //var sourceList = Helpers.GetArrayFromFile("ProgrammingQuestion-2 Q1 QuickSort.txt");
                        var sourceList = new List<int> { 3, 8, 2, 5, 1, 4, 7, 6, 12, 15, 9, 16, 88 };
                        Console.WriteLine("\tSource array : " + sourceList.Aggregate(string.Empty, (msg, current) => msg + (current + " ")));
                        //var pivotChoosingStrategy = new SelectFirstElementAsPivot();
                        //var pivotChoosingStrategy = new SelectLastElementAsPivot();
                        //var pivotChoosingStrategy = new SelectByMedianOfThreePivotRule();
                        var pivotChoosingStrategy = new MedianOfMediansDeterministicPivot();
                        var result = QuickSortProblem.QuickSort(sourceList, pivotChoosingStrategy);
                        Console.WriteLine("\tSorted array : " + result.DataArray.Aggregate(string.Empty, (msg, current) => msg + (current + " ")));
                        Console.WriteLine("Total number of comparisons : " + result.ComparisonsCount);
                        break;
                    }
                case 4:
                    {
                        Console.WriteLine("Q3.0 : Randomized Selection");
                        //var sourceList = Helpers.GetArrayFromFile("ProgrammingQuestion-2 Q1 QuickSort.txt");
                        var sourceList = new List<int> { 3, 8, 2, 5, 1, 4, 7, 6, 12, 15, 9, 16, 88 };
                        Console.WriteLine("\tSource array : " + sourceList.Aggregate(string.Empty, (msg, current) => msg + (current + " ")));
                        const int orderStatistic = 3;
                        var result = RandomizedSelectionProblem.RandomizedSelect(sourceList, orderStatistic);
                        Console.WriteLine("Selected item [" + orderStatistic + "] : " + result);
                        sourceList.Sort();
                        Console.WriteLine("\tSorted array : " + sourceList.Aggregate(string.Empty, (msg, current) => msg + (current + " ")));
                        Console.WriteLine("Check : " + sourceList[orderStatistic - 1]);
                        break;
                    }
                case 5:
                    {
                        Console.WriteLine("Q3.1 : Graphs");
                        var minCuts = new List<int>();
                        const int iter = 200;
                        const bool sampleData = false;
                        const string fileName = sampleData
                                                    ? "Copy of ProgrammingQuestion-3 Graphs.txt"
                                                    : "ProgrammingQuestion-3 Graphs.txt";
                        for (var i = 0; i < iter; i++)
                        {
                            Console.WriteLine("Iteration : " + (i + 1));
                            var graph = new GraphsProblem.Graph(fileName);
                            var minCut = graph.MinCut();
                            minCuts.Add(minCut);
                        }
                        Console.WriteLine("Min cuts for " + iter + " iterations : " + minCuts.Aggregate(string.Empty, (s, current) => s + " " + current));
                        Console.WriteLine("Min cut : " + minCuts.Min());
                        break;
                    }
                case 6:
                    {
                        Console.WriteLine("Q4 : Strongly connected components");
                        const bool sampleData = false;
                        const string fileName = sampleData
                                                    ? "Copy of ProgrammingQuestion-4 SCC.txt"
                                                    : "ProgrammingQuestion-4 SCC.txt";
                        var graph = new SCC.SCCGraph(fileName);
                        //var bfs = graph.BFS(1);
                        //Console.WriteLine("BFS : " + bfs.Aggregate(string.Empty, (s, current) => s+= "," + current).TrimStart(','));

                        //var shortestPathLength = graph.ShortestPath(1);
                        //Console.WriteLine("Shortest path : " + graph.ExploredVertices.Aggregate(string.Empty, (s, current) => s + " " + current));

                        //var dfs = graph.DFS(1);
                        //Console.WriteLine("DFS : " + dfs.Aggregate(string.Empty, (s, current) => s + "," + current).TrimStart(','));

                        var startTime = DateTime.Now;
                        var sccArray = graph.StronglyConnectedComponents();
                        var str = string.Empty;
                        var count = 0;
                        var sccSizes = new List<int>();
                        foreach (var scc in sccArray)
                        {
                            count++;
                            str += "SSC [" + count + "] Size [" + scc.Count + "] :\r\n";
                            str = scc.Aggregate(str, (current, vertex) => current + (vertex + " "));
                            str += "\r\n";
                            sccSizes.Add(scc.Count);
                        }
                        sccSizes.Sort();
                        str += "\r\nSizes : \r\n";
                        str = sccSizes.Aggregate(str, (current, sccSize) => current + (sccSize + "\r\n"));
                        var endTime = DateTime.Now;
                        var elapsedTime = endTime - startTime;
                        str += "\r\nElapsed time : " + elapsedTime.TotalSeconds + " seconds or " + elapsedTime.TotalMinutes + " minutes\r\n";
                        Console.WriteLine(str);
                        using (var sW = new StreamWriter("Results.txt"))
                        {
                            sW.WriteLine(str);
                        }
                        break;
                    }
                case 7:
                    {
                        Console.WriteLine("Q5 : Hashes");
                        const bool sampleData = false;
                        #region
                        //const string fileName = sampleData
                        //                            ? "Copy of ProgrammingQuestion-5 HashInt.txt"
                        //                            : "ProgrammingQuestion-5 HashInt.txt";
                        //var targetSums = sampleData
                        //                                 ? new List<int> { 150, 200, 250, 300, 350, 400, 450, 500, 550 }
                        //                                 : new List<int>
                        //                                       {
                        //                                           231552,
                        //                                           234756,
                        //                                           596873,
                        //                                           648219,
                        //                                           726312,
                        //                                           981237,
                        //                                           988331,
                        //                                           1277361,
                        //                                           1283379
                        //                                       }; 
                        #endregion
                        const string fileName = sampleData
                                                    ? "2sum.txt"
                                                    : "Programming Question - 6.1 HashInt.txt";
                        var targetSums = sampleData
                                             ? Enumerable.Range(2500, 1501).ToList()
                                             : Enumerable.Range(2500, 1501).ToList();

                        var sourceNumbersList = Helpers.GetArrayFromFile(fileName);
                        #region April 2012
                        //var resultBinaryString = targetSums
                        //    .Select(targetSum => HashesProblem.TestTargetSumBruteForce(sourceNumbersList, targetSum))
                        //    .Aggregate(string.Empty, (current, canBeFormed) => current + (canBeFormed ? "1" : "0"));
                        //var resultBinaryString = targetSums
                        //   .Select(targetSum => HashesProblem.TestTargetSum(sourceNumbersList, targetSum))
                        //   .Aggregate(string.Empty, (current, canBeFormed) => current + (canBeFormed ? "1" : "0"));
                        //Console.WriteLine(resultBinaryString); 
                        #endregion
                        var numberOfTargets = 0;
                        foreach (var targetSum in targetSums)
                        {
                            if (HashesProblem.TestTargetSumJune(sourceNumbersList, targetSum))
                            {
                                numberOfTargets++;
                            }
                        }
                        Console.WriteLine("Number of target t : " + numberOfTargets);
                        break;
                    }
                case 8:
                    {
                        Console.WriteLine("Q6 : Hashes - Median");
                        const bool sampleData = false;
                        const string fileName = sampleData
                                                    ? "Copy of ProgrammingQuestion-5 HashInt.txt"
                                                    : "Programming Question - 6.2 Median.txt";
                        //: "med3.txt";

                        var sourceNumbersList = Helpers.GetArrayFromFile(fileName);
                        //var sourceNumbersList = new List<int> { 4, 9, 8, 0, 13, 6, 4, 2, 14, 9, 11, 1, 10, 5, 7, 7, 3, 12 };
                        var medians = new List<int>();
                        for (var i = 0; i <= sourceNumbersList.Count; i++)
                        {
                            var currentList = sourceNumbersList.GetRange(0, i);
                            //Console.WriteLine("Current : ");
                            //var median = ListIntExtensions.GetMedian(currentList);
                            currentList.Sort();
                            //foreach (var i1 in currentList)
                            //{
                            //    Console.Write(i1 + " ");
                            //}
                            var median = currentList.Middle();
                            //Console.WriteLine("Median : " + median);
                            medians.Add(median);
                        }
                        var mediansString = medians.Aggregate(string.Empty, (s, current) => s += current + " ,");
                        //Console.WriteLine(mediansString);
                        var result = medians.Sum();// % 10000;
                        Console.WriteLine("Medians count : " + medians.Count + " Sum : " + result + " Answer : " + result % 10000);
                        break;
                    }
            }
            Console.WriteLine("Press <Enter> ...");
            Console.ReadLine();
        }
    }
}
