﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace ProgrammingQuestions
{
    class Helpers
    {
        public static List<int> GetArrayFromFile(string path)
        {
            using (var streamReader = new StreamReader(path))
            {
                var list = new List<int>();
                string currentLine;
                while ((currentLine = streamReader.ReadLine()) != null /*&& !string.IsNullOrEmpty(currentLine)*/)
                {
                    list.Add(int.Parse(currentLine));
                }
                return list;
            }
        }
    }

    struct InversionData
    {
        public long InversionsCount;
        public List<int> DataArray;
    }

    public struct Point
    {
        public int X;
        public int Y;
        public override string ToString()
        {
            return "(" + X + "," + Y + ")";
        }
    }

    // ClosestPair
    public static class TupleExtensions
    {
        public static Tuple<Point, Point> FindClosestPair(Tuple<Point, Point> firstPair, Tuple<Point, Point> secondPair)
        {
            return firstPair.EuclideanDistance() < secondPair.EuclideanDistance() ? firstPair : secondPair;
        }

        public static double EuclideanDistance(this Tuple<Point, Point> tuple)
        {
            return Math.Sqrt(Math.Pow(tuple.Item1.X - tuple.Item2.X, 2) + Math.Pow(tuple.Item1.Y - tuple.Item2.Y, 2));
        }
    }

    struct QuickSortData
    {
        public long ComparisonsCount;
        public List<int> DataArray;
    }

    // QuickSort
    public static class ListIntExtensions
    {
        public static void Swap(this List<int> list, int index1, int index2)
        {
            var tempValue = list[index1];
            list[index1] = list[index2];
            list[index2] = tempValue;
        }

        public static int Middle(this List<int> list)
        {
            if (list.Count == 0) return 0;
            if (list.Count == 1) return list[0];

            var elementsCount = list.Count;
            var middleIndex = elementsCount / 2;
            return (elementsCount % 2 == 1) ? list[middleIndex] : list[middleIndex - 1];
        }

        public static int MedianOfThree(this List<int> list)
        {
            if (list.Count != 3) throw new ArgumentException("Expecting three values in MedianOfThree");
            list.Sort();
            return list[1];
        }

        public static int GetMedian(List<int> sourceNumbers)
        {
            if (sourceNumbers == null || sourceNumbers.Count == 0)
                return 0;
            if (sourceNumbers.Count == 1) return sourceNumbers.First();
            sourceNumbers.Sort();
            var k = sourceNumbers.Count;
            var index = -1;
            if (k % 2 == 0)
            {
                index = k/2;
            }
            else
            {
                index = (k + 1)/2;
            }
            return sourceNumbers[index - 1];
        }
    }
}
