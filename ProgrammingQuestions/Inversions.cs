﻿using System.Collections.Generic;

namespace ProgrammingQuestions
{
    class Inversions
    {
        public static InversionData SortAndCountInversions(List<int> sourceList)
        {
            if (sourceList.Count <= 1)
            {
                return new InversionData { InversionsCount = 0, DataArray = sourceList };
            }
            var splitIndex = sourceList.Count / 2;
            var leftArrayPart = SortAndCountInversions(sourceList.GetRange(0, splitIndex));
            var rightArrayPart = SortAndCountInversions(sourceList.GetRange(splitIndex, sourceList.Count - splitIndex));
            var mergedArray = MergeAndCountInversions(leftArrayPart.DataArray, rightArrayPart.DataArray);
            return new InversionData { InversionsCount = leftArrayPart.InversionsCount + rightArrayPart.InversionsCount + mergedArray.InversionsCount, DataArray = mergedArray.DataArray };
        }

        private static InversionData MergeAndCountInversions(List<int> leftListPart, List<int> rightListPart)
        {
            var inversions = 0L;
            var elementsCount = leftListPart.Count + rightListPart.Count;
            var mergedArray = new List<int>();
            var i = 0;
            var j = 0;
            for (var k = 0; k < elementsCount; k++)
            {
                if (i >= leftListPart.Count || j >= rightListPart.Count) continue;
                if (leftListPart[i] < rightListPart[j])
                {
                    mergedArray.Add(leftListPart[i]);
                    i++;
                }
                else
                {
                    if (j < rightListPart.Count)
                    {
                        mergedArray.Add(rightListPart[j]);
                        j++;
                        inversions += leftListPart.Count - i;
                    }
                }
            }
            if (j < rightListPart.Count)
            {
                mergedArray.AddRange(rightListPart.GetRange(j, rightListPart.Count - j));
            }
            if (i < leftListPart.Count)
            {
                mergedArray.AddRange(leftListPart.GetRange(i, leftListPart.Count - i));
            }
            return new InversionData { InversionsCount = inversions, DataArray = mergedArray };
        }
    }
}
