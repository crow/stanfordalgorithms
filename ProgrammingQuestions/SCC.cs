﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.IO;
using System.Diagnostics;

namespace ProgrammingQuestions
{
    //public static class ListVertexExtensions
    //{
    //    public static ProgrammingQuestions.GraphsProblem.Vertex GetByLabel(this List<ProgrammingQuestions.GraphsProblem.Vertex> vertices, int label)
    //    {
    //        return vertices.Single(v => v.Label == label);
    //    }
    //}

    public class SCC
    {
        public class Arc
        {
            public int Start { get; set; }
            public int End { get; set; }

            public Arc (int start, int end)
	        {
                Start = start;
                End=end;
	        }

            public bool IsSelfCycle
            {
                get { return Start == End; }
            }

            public override string ToString()
            {
                return Start + "->" + End;
            }
        }

        public class SCCGraph
        {
            public List<Arc> Arcs { get; set; }

            public SCCGraph(string path)
            {
                using (var streamReader = new StreamReader(path))
                {
                    Arcs = new List<Arc>();
                    string currentLine;
                    while ((currentLine = streamReader.ReadLine()) != null /*&& !string.IsNullOrEmpty(currentLine)*/)
                    {
                        var numbers = Regex.Split(currentLine.Trim(), @"\s+");
                        var arcStart = int.Parse(numbers[0]);
                        var arcEnd = int.Parse(numbers[1]);
                        Arcs.Add(new Arc(arcStart, arcEnd));
                    }
                }
                Console.WriteLine("Graph arcs generated");
            }

            public List<int> ExploredVertices { get; set; }
            
            public bool WasExplored(int index)
            {
                return ExploredVertices.Contains(index); 
            }

            public void MarkAsExplored(int index)
            {
                ExploredVertices.Add(index);
                if (ExploredVertices.Count % 1000 == 0)
                {
                    Console.WriteLine(DateTime.Now + " " + ExploredVertices.Count);
                }
            }

            private Queue<int> bfsQueue;
            public IEnumerable<int> BFS(int startVertex)
            {
                bfsQueue = new Queue<int>();
                ExploredVertices = new List<int>();
                bfsQueue.Enqueue(startVertex);
                while (bfsQueue.Count > 0)
                {
                    var v = bfsQueue.Dequeue();
                    MarkAsExplored(v);
                    yield return v;
                    var vertexArcs = Arcs.Where(a => a.Start == v && !a.IsSelfCycle).ToList();
                    if (vertexArcs.Count > 0)
                    {
                        foreach (var arc in vertexArcs)
                        {
                            if (!WasExplored(arc.End))
                            {
                                MarkAsExplored(arc.End);
                                bfsQueue.Enqueue(arc.End);
                            }
                        }
                    }
                }
                //return ExploredVertices;
            }

            public int ShortestPath(int startVertex)
            {
                if (ExploredVertices == null) ExploredVertices = new List<int>();
                var lastVertex = Arcs.Last().End;
                if (startVertex == lastVertex)
                {
                    ExploredVertices.Add(startVertex);
                    return ExploredVertices.Count;
                }
                if (!WasExplored(startVertex))
                {
                    ExploredVertices.Add(startVertex);
                    var vertexArcs = Arcs.Where(a => a.Start == startVertex && !a.IsSelfCycle).ToList();
                    var startVertexAdjacentVertices = vertexArcs.Select(a => a.End).ToList();
                    var possibleNextVertices = startVertexAdjacentVertices.Where(v => !ExploredVertices.Contains(v)).ToList();
                    var possibleNextVerticesArcs = possibleNextVertices.Select(v => Arcs.Where(a => a.Start == v).ToList()).ToList();
                    if (possibleNextVerticesArcs.Count == 1 && possibleNextVerticesArcs[0].Count == 0)
                    {
                        var nextVertex = possibleNextVertices[0];
                        return ShortestPath(nextVertex);
                    }
                    //var filteredNextToNextVertices = new List<int>();
                    var aggressiveNextVertices = new List<int>();
                    foreach (var listArc in possibleNextVerticesArcs)
                    {
                        foreach (var arc in listArc)
                        {
                            if (!ExploredVertices.Contains(arc.End))
                            {
                                aggressiveNextVertices.Add(arc.Start);
                                //filteredNextToNextVertices.Add(arc.End);
                            }
                        }
                    }
                    // default strategy = select first from all possible next vertices
                    var nextVertex2 = aggressiveNextVertices.First();
                    Console.WriteLine(ExploredVertices);
                    return ShortestPath(nextVertex2);
                }
                else
                {
                    startVertex++;
                    return ShortestPath(startVertex);
                }
            }

            public Stack<int> dfsStack;
            public IEnumerable<int> DFS(int startVertex)
            {
                dfsStack = new Stack<int>();
                ExploredVertices = new List<int>();
                dfsStack.Push(startVertex);
                while (dfsStack.Count != 0)
                {
                    var v = dfsStack.Pop();
                    if (!WasExplored(v))
                    {
                        MarkAsExplored(v);
                        yield return v;
                        var neigbourVertices = Arcs.Where(arc => arc.Start == v && !arc.IsSelfCycle)
                                                   .Select(arc => arc.End)
                                                   .Where(vertex => !WasExplored(vertex))
                                                   .ToList();
                        neigbourVertices.Reverse();
                        neigbourVertices.ForEach(vertex => dfsStack.Push(vertex));
                    }
                }
                //bool repeat=true;
                //while (repeat)
                //{
                //    repeat = true;
                //    var lastVertex = Math.Max(Arcs.Select(arc => arc.Start).ToList().Max(), Arcs.Select(arc => arc.End).ToList().Max());
                //    var allVertices = Enumerable.Range(1, lastVertex).ToList();
                //    ExploredVertices.ForEach(vertex => allVertices.Remove(vertex));
                //    var missedVertices = allVertices;
                //    if (missedVertices.Count == 0) { repeat = false; break; }
                //    dfsStack.Push(missedVertices[0]);
                //    yield return -1;
                //    while (dfsStack.Count != 0)
                //    {
                //        var v = dfsStack.Pop();
                //        if (!WasExplored(v))
                //        {
                //            MarkAsExplored(v);
                //            yield return v;
                //            var neigbourVertices = Arcs.Where(arc => arc.Start == v && !arc.IsSelfCycle)
                //                                       .Select(arc => arc.End)
                //                                       .Where(vertex => !WasExplored(vertex))
                //                                       .ToList();
                //            neigbourVertices.Reverse();
                //            neigbourVertices.ForEach(vertex => dfsStack.Push(vertex));
                //        }
                //    }
                //}
                //return ExploredVertices;
            }

            public List<List<int>> StronglyConnectedComponents()
            {
                var G_reversed = Arcs.Select(arc => new Arc(arc.End, arc.Start)).ToList();
                Console.WriteLine("SCCDFS First pass");
                var firstPass = SCCDFS(G_reversed);
                var G_for_second_pass = Arcs.Select(arc => new Arc(firstPass[arc.Start], firstPass[arc.End])).ToList();
                Console.WriteLine("SCCDFS second pass");
                var secondPass = SCCDFS(G_for_second_pass);
                var sccsList = new List<List<int>>();
                var lst = new List<int>();
                foreach (var val in ExploredVertices)
                {
                    if (val != -1)
                    {
                        lst.Add(val);
                    }
                    if (val == -1)
                    {
                        var newList = new List<int>();
                        newList.AddRange(lst);
                        sccsList.Add(newList);
                        lst.Clear();
                    }
                }
                return sccsList;
            }

            private static int t;
            private static int s;
            private static int lastv;

            public Dictionary<int,int> SCCDFS(List<Arc> directedGraph)
            {
                dfsStack = new Stack<int>();
                ExploredVertices = new List<int>();
                var fi = new Dictionary<int, int>();
                var lastVertex = Math.Max(directedGraph.Select(arc => arc.Start).ToList().Max(), directedGraph.Select(arc => arc.End).ToList().Max());
                t = 0;
                s = -1;
                lastv = -1;
                for (var i = lastVertex; i > 0; i--)
                {
                    dfsStack.Push(i);
                    s = i; // s - leader of i
                    while (dfsStack.Count != 0)
                    {
                        var v = dfsStack.Pop();
                        //Debug.WriteLine("Vertex : " + v + " Explored : " + WasExplored(v));
                        if (!WasExplored(v))
                        {
                           // Console.WriteLine(string.Format("{0} is leader of {1}", s, v));
                            MarkAsExplored(v);
                            lastv = v;
                            t++;
                            fi.Add(v, t);
                            //yield return v;
                            var availableVertices = directedGraph.Where(arc => arc.Start == v && !arc.IsSelfCycle)
                                                       .Select(arc => arc.End)
                                                       .Where(vertex => !WasExplored(vertex))
                                                       .ToList();
                            //availableVertices.Reverse();
                            availableVertices.ForEach(vertex => dfsStack.Push(vertex));
                        }
                        if (dfsStack.Count == 0)
                        {
                            if (ExploredVertices.Last() != -1)
                            MarkAsExplored(-1);
                        }
                    }
                    //Debug.WriteLine("Ended loop for i=" + i);
                    //Debug.WriteLine("f(i) : " + fi.Aggregate(string.Empty, (seed, cur) => seed + " f(" + cur.Key + ")=" + cur.Value));
                }
                return fi;
            }
            
        }
    }
}
