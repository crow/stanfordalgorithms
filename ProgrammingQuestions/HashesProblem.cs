﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace ProgrammingQuestions
{
    class HashesProblem
    {

        public static bool TestTargetSumBruteForce(List<int> sourceNumbers, int targetSum)
        {
            return sourceNumbers.Any(t1 => sourceNumbers.Any(t => t1 + t == targetSum));
        }

        public static bool TestTargetSum(List<int> sourceNumbers, int targetSum)
        {
            var hashTable = new HashSet<int>();
            foreach (var sourceNumber in sourceNumbers.Where(sourceNumber => !hashTable.Contains(sourceNumber)))
            {
                hashTable.Add(sourceNumber);
            }
            return hashTable.Select(number => targetSum - number).Any(hashTable.Contains);
        }

        public static bool TestTargetSumJune(List<int> sourceNumbers, int targetSum)
        {
            var distinctSourceNumbers = new HashSet<int>();
            var count = 0;
            foreach (var sourceNumber in sourceNumbers.Where(sourceNumber => !distinctSourceNumbers.Contains(sourceNumber)))
            {
                distinctSourceNumbers.Add(sourceNumber);
            }
            if (targetSum % 100 == 0)
            {
                Console.WriteLine("Constructed hash for target sum " + targetSum);
            }
            var hash = new HashSet<Tuple<int, int>>();
            //using (var sW = new StreamWriter(targetSum + ".txt"))
            {
                foreach (var number in distinctSourceNumbers)
                {
                    var x = number;
                    var y = targetSum - number;
                    if (y <= 0 || x == y) continue;
                    if (distinctSourceNumbers.Contains(y))
                    {
                        var t = new Tuple<int, int>(x, y);
                        var reversedt = new Tuple<int, int>(y, x);
                        if (!hash.Contains(t) && !hash.Contains(reversedt))
                        {
                            hash.Add(t);
                            return true;
                            //sW.WriteLine(x + "," + y);
                        }
                    }
                }
            }
            return false;
        }
    }
}
