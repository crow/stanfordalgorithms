﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace ProgrammingQuestions
{
    public class GraphsProblem
    {
        public class Vertex
        {
            public int Label { get; set; }
            public List<int> AdjacentVerticesIndexes;
            public List<Vertex> AdjacentVertices;

            public override string ToString()
            {
                return "[" + Label + "] AV : " + AdjacentVerticesIndexes.Aggregate(string.Empty, (s, current) => s + (current + " "));
            }
            
            public void RemoveAdjacentVertex(Vertex vertex)
            {
                // multiple remove if vertex had few parallel edges
                while (AdjacentVerticesIndexes.Contains(vertex.Label))
                {
                    AdjacentVerticesIndexes.Remove(vertex.Label);
                    var removeCandidate = AdjacentVertices.First(v => v.Label == vertex.Label);
                    AdjacentVertices.Remove(removeCandidate);
                    Validate();
                }
                // TODO comment
                //Validate();
            }

            private void Validate()
            {
                if (AdjacentVertices.Count != AdjacentVerticesIndexes.Count)
                    throw new Exception("Vertex [" + Label + "] error");
            }

            public void AddAdjacentVertex(Vertex vertex)
            {
                if (vertex == null) throw new Exception();
                if (AdjacentVerticesIndexes == null) AdjacentVerticesIndexes = new List<int>();
                AdjacentVerticesIndexes.Add(vertex.Label);
                if (AdjacentVertices == null) AdjacentVertices = new List<Vertex>();
                AdjacentVertices.Add(vertex);
            }

            public void ReplaceContactedVertexWith(Vertex vertex)
            {
                foreach (var adjacentVertex in AdjacentVertices)
                {
                    adjacentVertex.RemoveAdjacentVertex(this);
                    adjacentVertex.AddAdjacentVertex(vertex);
                    vertex.AddAdjacentVertex(adjacentVertex);
                }
            }

            public bool WasTraversed { get; set; }
        }

        public class Edge
        {
            public Vertex Start { get; set; }
            public Vertex End { get; set; }

            public override string ToString()
            {
                return Start.Label + "->" + End.Label;
            }

            public void Validate()
            {
                if(Start.AdjacentVertices.Count != Start.AdjacentVerticesIndexes.Count)
                    throw new Exception("Edge [" + ToString() + "] Start error");
                if (End.AdjacentVertices.Count != End.AdjacentVerticesIndexes.Count)
                    throw new Exception("Edge [" + ToString() + "] End error");
            }
        }

        public class Graph
        {
            internal static int _initialVerticesCount;
            internal static int _currentIteration;
            public List<Vertex> Vertices { get; set; }
            public List<Edge> Edges { get; set; }

            public Graph(string path)
            {
                using (var streamReader = new StreamReader(path))
                {
                    var verticesList = new List<Vertex>();
                    string currentLine;
                    while ((currentLine = streamReader.ReadLine()) != null /*&& !string.IsNullOrEmpty(currentLine)*/)
                    {
                        var numbers = Regex.Split(currentLine.Trim(), @"\s+");
                        var vertex = new Vertex { Label = int.Parse(numbers[0]) };

                        var adjacentVerticesIndexes = new List<int>();
                        for (int i = 1; i < numbers.Length; i++)
                        {
                            adjacentVerticesIndexes.Add(int.Parse(numbers[i]));
                        }
                        vertex.AdjacentVerticesIndexes = adjacentVerticesIndexes;
                        verticesList.Add(vertex);
                    }
                    Vertices = verticesList;
                    for (int index = 0; index < Vertices.Count; index++)
                    {
                        var vertex = Vertices[index];
                        var count = vertex.AdjacentVerticesIndexes.Count;
                        //Console.WriteLine("Vertex : " + vertex.Label + " AV : " + count);
                        for (int i = 0; i < count; i++)
                        {
                            var adjacentVertexIndex = vertex.AdjacentVerticesIndexes[i];
                            //Console.WriteLine("Adjacent : " + adjacentVertexIndex);
                            if (vertex.AdjacentVertices == null) vertex.AdjacentVertices = new List<Vertex>();
                            vertex.AdjacentVertices.Add(Vertices.Single(v => v.Label == adjacentVertexIndex));
                        }
                    }
                    BuildEdges();
                    _currentIteration = 0;
                    _initialVerticesCount = Vertices.Count;
                }
            }

            public Graph()
            { }

            internal void BuildEdges()
            {
                if (Vertices == null) return;
                var edgesDic = new Dictionary<string, Edge>();
                foreach (var vertex in Vertices)
                {
                    if (vertex.Label % 50000 == 0)
                    {
                        Console.WriteLine("Building edges for vertex : " + vertex.Label);
                    }
                    foreach (var adjacentVertex in vertex.AdjacentVertices)
                    {
                        // ReSharper disable SpecifyACultureInStringConversionExplicitly
                        var edgeId = vertex.Label.ToString() + "-" + adjacentVertex.Label.ToString();
                        var edgeIdReversed = adjacentVertex.Label.ToString() + "-" + vertex.Label.ToString();
                        // ReSharper restore SpecifyACultureInStringConversionExplicitly
                        if (!edgesDic.ContainsKey(edgeId) && !edgesDic.ContainsKey(edgeIdReversed))
                        {
                            edgesDic.Add(edgeId, new Edge { Start = vertex, End = adjacentVertex });
                        }
                    }
                }
                Edges = edgesDic.Values.ToList();
            }

            internal Edge SelectRandomEdge()
            {
                var r = new Random();
                var randomIndex = r.Next(Edges.Count);
                return Edges[randomIndex];
            }

            internal void RemoveEdge(Edge edgeToRemove)
            {
                _currentIteration++;
                var newVertexIndex = _initialVerticesCount + _currentIteration;
                var newVertex = new Vertex { Label = newVertexIndex };
                
                edgeToRemove.Start.RemoveAdjacentVertex(edgeToRemove.End);
                edgeToRemove.End.RemoveAdjacentVertex(edgeToRemove.Start);

                Vertices.Remove(edgeToRemove.Start);
                Vertices.Remove(edgeToRemove.End);
                Vertices.Add(newVertex);
                //Console.WriteLine("Added vertex : " + newVertex.Label + "\r\n");

                edgeToRemove.Start.ReplaceContactedVertexWith(newVertex);
                edgeToRemove.End.ReplaceContactedVertexWith(newVertex);
                #region old
                //foreach (var adjacentToStart in edgeToRemove.Start.AdjacentVertices)
                //{
                //    adjacentToStart.RemoveAdjacentVertex(edgeToRemove.Start);
                //    adjacentToStart.AddAdjacentVertex(newVertex);
                //    newVertex.AddAdjacentVertex(adjacentToStart);
                //}
                //foreach (var adjacentToEnd in edgeToRemove.End.AdjacentVertices)
                //{
                //    adjacentToEnd.RemoveAdjacentVertex(edgeToRemove.End);
                //    adjacentToEnd.AddAdjacentVertex(newVertex);
                //    newVertex.AddAdjacentVertex(adjacentToEnd);
                //}
                //Console.WriteLine("[" + _currentIteration + "] Fusing vertices : " + edgeToRemove.Start.Label + " and " + edgeToRemove.End.Label + " into " + newVertex.Label); 
                #endregion
                //Console.WriteLine("[" + _currentIteration + "] Removing edge : " + edgeToRemove);
                Edges.Remove(edgeToRemove);
                var edgesHadStartInDeletedVertex = Edges.Where(e => e.Start == edgeToRemove.Start || e.Start == edgeToRemove.End).ToList();
                foreach (var edge in edgesHadStartInDeletedVertex)
                {
                    var oldEdge = edge.ToString();
                    edge.Start = newVertex;
                    //Console.WriteLine("Changing edge : " + oldEdge + " into " + edge);
                }
                var edgesHadEndInDeletedVertex = Edges.Where(e => e.End == edgeToRemove.Start || e.End == edgeToRemove.End).ToList();
                foreach (var edge in edgesHadEndInDeletedVertex)
                {
                    var oldEdge = edge.ToString();
                    edge.End = newVertex;
                    //Console.WriteLine("Changing edge : " + oldEdge + " into " + edge);
                }
               
                var loopEdges = Edges.Where(edge => edge.Start == edge.End).ToList();
                foreach (var edge in loopEdges)
                {
                    Edges.Remove(edge);
                }
            }

            public int MinCut()
            {
                if (Vertices.Count == 2) return Edges.Count;
                var currentEdgeToContact = SelectRandomEdge();
                RemoveEdge(currentEdgeToContact);
                return MinCut();
            }
        }
    }
}
