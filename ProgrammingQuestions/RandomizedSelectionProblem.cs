﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ProgrammingQuestions
{
    public class RandomizedSelectionProblem
    {
        public static int RandomizedSelect(List<int> sourceList, int statisticsOrder)
        {
            if (sourceList.Count == 1)
            {
                return sourceList[0];
            }
            
            var randomPivotStrategy = new SelectUniformlyRandomPivot();
            var pivotElement = randomPivotStrategy.ChoosePivot(sourceList);
            if (sourceList[0] != pivotElement)
            {
                sourceList.Swap(0, sourceList.IndexOf(pivotElement));
            }
            var partitionedList = Partition(sourceList, pivotElement);
            Console.WriteLine("\tPartitioned list : " + sourceList.Aggregate(string.Empty, (msg, current) => msg + (current + " ")));
                        
            var j = partitionedList.IndexOf(pivotElement) + 1;
            if (statisticsOrder == j)
            {
                return pivotElement;
            }
            int start = -1;
            int count = -1;
            if (j > statisticsOrder)
            {
                //RandomizedSelect(partitionedList.GetRange(0, j), statisticsOrder);
                start = 0;
                count = j - 1;
                Console.WriteLine("\t\tPicking <left> part. Statistic=" + statisticsOrder);
            }
            if (j < statisticsOrder)
            {
                //RandomizedSelect(partitionedList.GetRange(j + 1, partitionedList.Count - j - 1), statisticsOrder - j);
                start = 1;
                count = partitionedList.Count - j;
                statisticsOrder -= j;
                Console.WriteLine("\t\tPicking <right> part. Statistic=" + statisticsOrder);
            }
            if (start == -1 || count == -1) throw new ArgumentOutOfRangeException();
            var newList = partitionedList.GetRange(start, count);
            Console.WriteLine("\tNew list : " + newList.Aggregate(string.Empty, (msg, current) => msg + (current + " ")));
            return RandomizedSelect(newList, statisticsOrder);
        }

        // in-place partition
        public static List<int> Partition(List<int> sourceList, int pivot)
        {
            Console.WriteLine("\tPartitioning with p=[" + pivot + "] : " + sourceList.Aggregate(string.Empty, (msg, current) => msg + (current + " ")));
            var pivotIndex = sourceList.IndexOf(pivot);
            for (var i = pivotIndex + 1; i < sourceList.Count; i++)
            {
                for (var j = i; j < sourceList.Count; j++)
                {
                    if (sourceList[j] > pivot) { /*nothing to do*/ }
                    if (sourceList[j] < pivot)
                    {
                        sourceList.Swap(i, j);
                        i++;
                    }
                    if (j != sourceList.Count - 1) continue;
                    sourceList.Swap(pivotIndex, i - 1);
                    i = sourceList.Count;
                }
            }
            //Console.WriteLine("\t\tResult : " + sourceList.Aggregate(string.Empty, (msg, current) => msg + (current + " ")));
            return sourceList;
        }
    }

    public class SelectUniformlyRandomPivot : IPivotChoosingStrategy
    {
        public int ChoosePivot(List<int> sourceList)
        {
            var r = new Random();
            var index = r.Next(0, sourceList.Count - 1);
            return sourceList[index];
        }
    }
}
