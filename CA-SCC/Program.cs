﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using QuickGraph;
using QuickGraph.Algorithms.ConnectedComponents;
using QuickGraph.Algorithms;
using QuickGraph.Algorithms.ShortestPath;
using QuickGraph.Algorithms.Observers;

namespace CA_SCC
{
    class Program
    {
        static void Main()
        {
            //CalculateSCCSizes();
            CalculateShortestPath();
        }

        public static void CalculateSCCSizes()
        {
            const bool sampleData = false;
            const string fileName = sampleData
                                                    ? "Copy of ProgrammingQuestion-4 SCC.txt"
                                                    : "ProgrammingQuestion-4 SCC.txt";
            AdjacencyGraph<int, SEdge<int>> graph;
            Console.WriteLine("[" + DateTime.Now + "] Starting reading graph...");
            using (var streamReader = new StreamReader(fileName))
            {
                var edges = new List<SEdge<int>>();
                string currentLine;
                while ((currentLine = streamReader.ReadLine()) != null /*&& !string.IsNullOrEmpty(currentLine)*/)
                {
                    var numbers = Regex.Split(currentLine.Trim(), @"\s+");
                    var arcStart = int.Parse(numbers[0]);
                    var arcEnd = int.Parse(numbers[1]);
                    edges.Add(new SEdge<int>(arcStart, arcEnd));
                }
                graph = edges.ToAdjacencyGraph<int, SEdge<int>>();

            }
            Console.WriteLine("[" + DateTime.Now + "] Graph constructed : Vertices=" + graph.VertexCount + " Edges=" + graph.Edges.Count());
            var strong = new StronglyConnectedComponentsAlgorithm<int, SEdge<int>>(graph);
            var startTime = DateTime.Now;
            strong.Compute();
            var endTime = DateTime.Now;
            Console.WriteLine("[" + DateTime.Now + "] SCC : Count=" + strong.ComponentCount);

            var sccComponents = strong.Components;
            var sccGroupIndexes = sccComponents.Values.ToList();
            var groupedSccIndexes = sccGroupIndexes.GroupBy(i => i);
            var sccSizes = groupedSccIndexes.Select(@group => @group.Count()).ToList();
            sccSizes.Sort();

            var sccsCount = sccSizes.Count;
            var lastTenLargestSccs = sccSizes.Skip(sccsCount - 10).Take(10);

            var resultMessage = string.Empty;
            resultMessage += "SCC sizes : \r\n";
            resultMessage = lastTenLargestSccs.Aggregate(resultMessage, (current, sccSize) => current + (sccSize + "\r\n"));
            var elapsedTime = endTime - startTime;
            resultMessage += "Elapsed time : " + elapsedTime.TotalSeconds + " seconds or " + elapsedTime.TotalMinutes + " minutes\r\n";
            Console.WriteLine(resultMessage);

            using (var sW = new StreamWriter("Results.txt"))
            {
                sW.WriteLine(resultMessage);
            }

            Console.WriteLine("Press <Enter> ...");
            Console.ReadLine();
        }

        public static void CalculateShortestPath()
        {
            const bool sampleData = false;
            const string fileName = sampleData
                                                    ? "Copy of ProgrammingQuestion-4 SCC.txt"
                                                    : "dijkstraData.txt";
            var targetVertexes = new List<int> { 7, 37, 59, 82, 99, 115, 133, 165, 188, 197 };
            AdjacencyGraph<int, SEdge<int>> graph;
            var edgeCostDictionary = new Dictionary<SEdge<int>, double>();
            Console.WriteLine("[" + DateTime.Now + "] Starting reading graph...");
            using (var streamReader = new StreamReader(fileName))
            {
                var edges = new List<SEdge<int>>();
                string currentLine;
                while ((currentLine = streamReader.ReadLine()) != null /*&& !string.IsNullOrEmpty(currentLine)*/)
                {
                    var numbers = Regex.Split(currentLine.Trim(), @"\s+");
                    var arcStart = int.Parse(numbers[0]);
                    for (var i = 1; i < numbers.Length; i++)
                    {
                        var edgeData = numbers[i].Split(',');
                        var arcEnd = int.Parse(edgeData[0]);
                        var edgeCost = int.Parse(edgeData[1]);
                        var edge = new SEdge<int>(arcStart, arcEnd);
                        edgeCostDictionary.Add(edge, edgeCost);
                        edges.Add(edge);
                    }
                }
                graph = edges.ToAdjacencyGraph<int, SEdge<int>>();
            }
            Console.WriteLine("[" + DateTime.Now + "] Graph constructed : Vertices=" + graph.VertexCount + " Edges=" + graph.Edges.Count());

            Func<SEdge<int>, double> edgeCostfunc = AlgorithmExtensions.GetIndexer(edgeCostDictionary);

            var dijkstra = new DijkstraShortestPathAlgorithm<int, SEdge<int>>(graph, edgeCostfunc);
            // Attach a Vertex Predecessor Recorder Observer to give us the paths
            var predecessors = new VertexPredecessorRecorderObserver<int, SEdge<int>>();
            using (predecessors.Attach(dijkstra))
            // Run the algorithm with A set to be the source
            dijkstra.Compute(1);

            var distances = new Dictionary<int, double>();
            foreach (var v in graph.Vertices)
            {
                double distance = 0;
                var vertex = v;
                SEdge<int> predecessor;
                while (predecessors.VertexPredecessors.TryGetValue(vertex, out predecessor))
                {
                    distance += edgeCostDictionary[predecessor];
                    vertex = predecessor.Source;
                }
                //Console.WriteLine("1 ->{0}: {1}", v, distance);
                distances.Add(v, distance);
            }

            //foreach (var distancekvp in distances)
            //{
            //    if (targetVertexes.Contains(distancekvp.Key))
            //    {
            //        Console.WriteLine("1->" + distancekvp.Key + " : " + distancekvp.Value);
            //    }
            //}

            var resultMessage = string.Empty;
            foreach (var targetVertex in targetVertexes)
            {
                var distance = 1000000.0;
                if (!distances.TryGetValue(targetVertex, out distance))
                {
                }
                resultMessage += distance + ",";
            }
            resultMessage = resultMessage.TrimEnd(',');
            Console.WriteLine(resultMessage);

            using (var sW = new StreamWriter("DijkstraResults.txt"))
            {
                sW.WriteLine(resultMessage);
            }

            Console.WriteLine("Press <Enter> ...");
            Console.ReadLine();
        }
    }
}
